package ua.ithillel.java.elementary.socket.http.client;

import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Value
@Builder(toBuilder = true)
public class SimpleHttpResponse {
    int statusCode;
    String statusText;
    String payload;
    Map<String,String> headers;
}
